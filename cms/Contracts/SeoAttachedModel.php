<?php

namespace Cms\Contracts;

use App\Models\SeoMeta;

interface SeoAttachedModel
{
    /**
     * Determine if the current model exists in database.
     */
    public function existsInDatabase(): bool;

    /**
     * Get an attribute from the model.
     *
     * @param  string  $key
     * @return mixed
     */
    public function getAttribute($key);

    /**
     * Get the value of the model's primary key.
     *
     * @return mixed
     */
    public function getKey();

    /**
     * Get the SEO Meta instance matched with the current application locale.
     */
    public function getSeoMetaAttribute(string $locale = null): SeoMeta;
}

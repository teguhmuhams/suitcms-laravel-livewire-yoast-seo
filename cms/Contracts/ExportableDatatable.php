<?php

namespace Cms\Contracts;

use Illuminate\Database\Eloquent\Builder;

interface ExportableDatatable
{
    /**
     * Get eloquent query builder for data export processing.
     */
    public function getExportQuery(): Builder;

    /**
     * Get the export heading row.
     */
    public function getExportHeadings(): array;
}

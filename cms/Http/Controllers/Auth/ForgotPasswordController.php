<?php

namespace Cms\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

class ForgotPasswordController extends Controller
{
    public function sendResetLinkEmail(Request $request): \Illuminate\Http\RedirectResponse
    {
        $this->validateEmail($request);

        $status = Password::broker('admins')->sendResetLink(
            $request->only('email')
        );

        return $status === Password::RESET_LINK_SENT
            ? back()->with(['status' => __($status)])
            : back()->withErrors(['email' => __($status)]);
    }

    /**
     * Display the form to request a password reset link.
     *
     * @return mixed
     */
    public function showLinkRequestForm()
    {
        return view('cms::auth.passwords.email');
    }

    /**
     * Validate the email for the given request.
     *
     *
     * @return void
     */
    protected function validateEmail(Request $request)
    {
        $rules = [
            'email' => 'required|email|min:11',
        ];

        if (config('cms.captcha_enabled')) {
            $rules['g-recaptcha-response'] = 'required|captcha';
        }

        $request->validate($rules);
    }
}

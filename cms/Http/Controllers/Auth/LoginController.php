<?php

namespace Cms\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Cms\Auth\ThrottlesLogins;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    use ThrottlesLogins;

    public function username(): string
    {
        return 'email';
    }

    /**
     * Show the application's login form.
     *
     * @return mixed
     */
    public function showLoginForm()
    {
        return view('cms::auth.login');
    }

    /**
     * Get the guard to be used during authentication.
     */
    protected function guard(): \Illuminate\Contracts\Auth\StatefulGuard|\Illuminate\Contracts\Auth\Guard
    {
        return \Auth::guard(config('cms.guard'));
    }

    protected function validateLogin(Request $request): void
    {
        $rules = [
            'email' => 'required|string',
            'password' => 'required|string',
        ];

        if (config('cms.captcha_enabled')) {
            $rules['g-recaptcha-response'] = 'required|captcha';
        }

        $request->validate($rules);
    }

    /**
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if (
            method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)
        ) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            if ($request->hasSession()) {
                $request->session()->put('auth.password_confirmed_at', time());
            }

            $request->session()->regenerate();

            $this->clearLoginAttempts($request);

            return $request->wantsJson()
                ? new JsonResponse([], 204)
                : redirect()->route('cms.home');
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ]);
    }

    protected function attemptLogin(Request $request): bool
    {
        return $this->guard()->attempt(
            $request->only('email', 'password'),
            $request->boolean('remember')
        );
    }

    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return $request->wantsJson()
            ? new JsonResponse([], 204)
            : redirect(route('cms.auth.login'));
    }
}

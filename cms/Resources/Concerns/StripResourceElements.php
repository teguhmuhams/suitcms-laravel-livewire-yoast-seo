<?php

namespace Cms\Resources\Concerns;

trait StripResourceElements
{
    /**
     * Strip some elements from the given resource collection.
     */
    protected function stripElementsFromCollection(array $collection, array $elements): array
    {
        return collect($collection)->map(static function ($value) use ($elements) {
            foreach ($elements as $element) {
                if (isset($value[$element])) {
                    unset($value[$element]);
                }
            }

            return $value;
        })->toArray();
    }

    /**
     * Strip some elements from the given resource.
     */
    protected function stripElementsFromResource(array $resource, array $elements): array
    {
        foreach ($elements as $element) {
            if (isset($resource[$element])) {
                unset($resource[$element]);
            }
        }

        return $resource;
    }
}

<?php

namespace App\Traits;

use Illuminate\Validation\Validator;

trait FormValidationTrait
{
    public function validateData(string $module, array $routeParam): void
    {
        $this->withValidator(function (Validator $validator) use ($module, $routeParam) {
            $validator->after(function (Validator $validator) use ($module, $routeParam) {
                if (count($validator->errors()->all()) > 0) {
                    $route = $this->operation === 'create'
                        ? route('cms.'.$module.'.create')
                        : route('cms.'.$module.'.edit', $routeParam);

                    return redirect()->to($route)
                        ->with(['cmsErrors' => $validator->errors()->all()]);
                }
            });
        })->validate();
    }

    public function setTranslatableAttributes(string $model): void
    {
        if (in_array($this->operation, ['create', 'update'])) {
            $this->translations = $this->{$model}->getAllTranslatableValues();
            if (empty($this->{$model}->getAllTranslatableValues())) {
                $this->translations = collect($this->{$model}->getTranslatableAttributes())
                    ->mapWithKeys(
                        fn ($item) => [$item => ['en' => null, 'id' => null]]
                    )
                    ->toArray();
            }
            $this->translations = collect($this->{$model}->getTranslatableAttributes())
                ->mapWithKeys(
                    fn ($item) => [$item => ['en' => $this->translations[$item]['en'] ?? null, 'id' => $this->translations[$item]['id'] ?? null]]
                )
                ->toArray();
        }
    }
}

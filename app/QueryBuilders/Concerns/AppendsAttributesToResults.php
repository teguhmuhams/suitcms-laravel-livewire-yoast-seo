<?php

namespace App\QueryBuilders\Concerns;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Spatie\QueryBuilder\Exceptions\InvalidAppendQuery;

trait AppendsAttributesToResults
{
    /** @var \Illuminate\Support\Collection */
    protected $allowedAppends;

    /**
     * @param  mixed  $appends
     * @return \App\QueryBuilders\QueryBuilder
     */
    public function allowedAppends($appends): self
    {
        $appends = is_array($appends) ? $appends : func_get_args();

        $this->allowedAppends = collect($appends);

        $this->ensureAllAppendsExist();

        return $this;
    }

    /**
     * @return mixed
     */
    protected function addAppendsToResults(Collection $results)
    {
        return $results->each(function (Model $result) { //@phpstan-ignore-line
        return $result->append($this->request->appends()->toArray());
        });
    }

    /**
     * @param  Collection  $results
     * @return mixed
     */
    protected function addAppendsToCursor($results)
    {
        return $results->each(function (Model $result) { //@phpstan-ignore-line
        return $result->append($this->request->appends()->toArray());
        });
    }

    /**
     * @return void
     */
    protected function ensureAllAppendsExist()
    {
        $appends = $this->request->appends();

        $diff = $appends->diff($this->allowedAppends);

        if ($diff->count()) {
            throw InvalidAppendQuery::appendsNotAllowed($diff, $this->allowedAppends);
        }
    }
}

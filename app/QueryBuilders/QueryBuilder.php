<?php

namespace App\QueryBuilders;

use App\QueryBuilders\Concerns\AppendsAttributesToResults;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\CursorPaginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;

class QueryBuilder extends \Spatie\QueryBuilder\QueryBuilder
{
    use AppendsAttributesToResults;

    /**
     * @param  string  $name
     * @param  array  $arguments
     * @return $this|QueryBuilder|Model|CursorPaginator|LengthAwarePaginator|Paginator|Collection|mixed
     */
    public function __call($name, $arguments)
    {
        $result = $this->forwardCallTo($this->subject, $name, $arguments);

        /*
         * If the forwarded method call is part of a chain we can return $this
         * instead of the actual $result to keep the chain going.
         */
        if ($result === $this->subject) {
            return $this;
        }

        if ($result instanceof Model) {
            $this->addAppendsToResults(collect([$result]));
        }

        if ($result instanceof Collection) {
            $this->addAppendsToResults($result);
        }

        if ($result instanceof LengthAwarePaginator || $result instanceof Paginator || $result instanceof CursorPaginator) {
            $this->addAppendsToResults(collect($result->items()));
        }

        return $result;
    }
}

@php
    $labelName = $labelName ?? 'Content';
    $name = $name ?? 'content';
    $locale = $locale ?? 'en';
    $name = 'translations.' . $name . '.' . $locale;
@endphp

<x-input.tinymce
    labelName="{{ ucwords(str_replace('_', ' ', $labelName)) }}"
    id="{{ str_replace('.', '-', $name) }}"
    name="{{ $name }}"
    wire:model.defer="{{ $name }}"
/>

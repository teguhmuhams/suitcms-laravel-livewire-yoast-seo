<div class="mt-16 mb-16">
    <div class="mb-8">
        <h3>Attached SEO Meta Data</h3>
    </div>

    @multilingual
        {!! CmsForm::text('seoMeta.seo_title', $component->getSeoFormAttribute())->setTitle('Title') !!}
        {!! CmsForm::textarea('seoMeta.seo_description', $component->getSeoFormAttribute())->setTitle('Description') !!}
        {!! CmsForm::select('seoMeta.open_graph_type', $openGraphTypes, $component->getSeoFormAttribute()) !!}

        @php
            $localeKey = config('i18n.language_key', 'language');
        @endphp
        <div class="form-group mt-12">
            @if ($component->getSeoFormOperation() === 'view')
                @if (
                    $component->getAttachedModel()->getSeoMetaAttribute($_locale->{$localeKey})->exists &&
                        $component->getAttachedModel()->getSeoMetaAttribute($_locale->{$localeKey})->getFirstMediaUrl('seo_image', 'seo_image_small'))
                    <label for="seoImage" style="display: block;">SEO Image</label>
                    <img src="{{ asset($component->getAttachedModel()->getSeoMetaAttribute($_locale->{$localeKey})->getFirstMediaUrl('seo_image', 'seo_image_small')) }}"
                        style="border: 1px solid #333;" />
                @endif
            @else
                <label for="seoMedia">SEO Image</label>
                <x-media-library-attachment name="seoMedia{{ ucfirst($_locale->{$localeKey}) }}" rules="mimes:jpeg,png" />
                <div class="font-size-sm mt-2 text-info">It is recommended to upload an image with 1600x800 resolution.
                </div>

                @if (
                    $component->getAttachedModel()->getSeoMetaAttribute($_locale->{$localeKey})->exists &&
                        $component->getAttachedModel()->getSeoMetaAttribute($_locale->{$localeKey})->getFirstMediaUrl('seo_image', 'seo_image_small'))
                    <div class="mt-6">
                        <img src="{{ asset($component->getAttachedModel()->getSeoMetaAttribute($_locale->{$localeKey})->getFirstMediaUrl('seo_image', 'seo_image_small')) }}"
                            style="border: 1px solid #333;" />
                    </div>
                @endif
            @endif
        </div>

        <button type="button" class="btn btn-primary" onclick="getSeoAssessment()">Get SEO Assessment</button>

        <div id="seo_assessment" class="my-3 col-md-12">
        </div>

        <div id="content_assessment" class="my-3 col-md-12">
        </div>
    @endmultilingual

</div>

<script>
    function getSeoAssessment() {
        @if ($component->getSeoFormOperation() === 'view')
            const text = document.getElementById('staticPage.content').value
        @else
            const text = tinymce.activeEditor.getContent();
        @endif

        const title = document.getElementById('seoMeta_seo_title_en').value
        const keyword = document.getElementById('seoMeta_seo_title_en').value
        const description = document.getElementById('seoMeta_seo_description_en').value
        const locale = "en_US"

        const paper = new YoastSeo.Paper(text, {
            title: title,
            keyword: keyword,
            description: description,
            locale: locale,
        });

        const contentAssessor = new YoastSeo.ContentAssessor(i18n());
        const seoAssessor = new YoastSeo.SeoAssessor(i18n());

        contentAssessor.assess(paper);
        seoAssessor.assess(paper);

        const final_scores = getScores(seoAssessor, contentAssessor);

        seo = final_scores.seo;
        content = final_scores.content;

        const seoAssessment = document.getElementById('seo_assessment')

        seoAssessment.innerHTML = `<h3>SEO assessments</h3>
        <ul class="bulleted-list" id="seoList">
        </ul>
        `
        const contentAssessment = document.getElementById('content_assessment')

        contentAssessment.innerHTML = `<h3>Content assessments</h3>
        <ul class="bulleted-list" id="contentList">
        </ul>
        `

        const seoList = document.getElementById('seoList');
        seo.forEach(element => {
            const liElement = document.createElement('li');
            liElement.classList.add('col-md-12', 'p-2', 'pl-0', 'score', 'seo-score-text')
            if (element.rating === 'good')
                liElement.classList.add('seo-score-icon-good')
            else if (element.rating === 'bad')
                liElement.classList.add('seo-score-icon-bad')
            else if (element.rating === 'ok')
                liElement.classList.add('seo-score-icon-ok')
            liElement.innerHTML = element.text + " score: " + element.score
            seoList.append(liElement)
        });

        const contentList = document.getElementById('contentList');

        content.forEach(element => {
            const liElement = document.createElement('li');
            liElement.classList.add('col-md-12', 'p-2', 'pl-0', 'score', 'seo-score-text')
            if (element.rating === 'good')
                liElement.classList.add('seo-score-icon-good')
            else if (element.rating === 'bad')
                liElement.classList.add('seo-score-icon-bad')


            liElement.innerHTML = element.text + " score: " + element.score
            contentList.append(liElement)
        });
    }

    function getScores(seoAssessor, contentAssessor) {
        return {
            seo: new Presenter().getScoresWithRatings(seoAssessor),
            content: new Presenter().getScoresWithRatings(contentAssessor),
        };
    }

    function getScoresAsHTML(scores) {
        return new Presenter().getScoresAsHTML(h, scores);
    }

    function i18n() {
        return new Jed({
            domain: `js-text-analysis`,
            locale_data: {
                "js-text-analysis": {
                    "": {}
                },
            },
        });
    }
</script>

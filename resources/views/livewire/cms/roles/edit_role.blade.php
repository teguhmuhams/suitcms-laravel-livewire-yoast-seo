@section('additional_scripts')
<script type="text/javascript">
    window.resourceUrl = "{{ route('cms.roles.index') }}'";
</script>
@endsection

<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <livewire:cms.nav.breadcrumb :items="$this->breadcrumbItems" />

            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Edit Role #{{ $role->getKey() }}</h3>
                </div>
                <div class="card-body">
                    @include('cms::_partials.alert')

                    <form class="form" wire:submit.prevent="save">
                        {{ CmsForm::setErrorBag($errors) }}

                        {!! CmsForm::text('role.name') !!}
                        {!! CmsForm::text('role.guard_name') !!}

                        @foreach ($menuPermissions as $item)
                            @if ($this->canManageAtLeastOneNode($item))
                                <div class="mt-16">
                                    <h3>{{ data_get($item, 'title') }} Permissions</h3>
                                </div>

                                <table class="table mt-5 mb-8 table-bordered table-responsive-sm">
                                    <tr>
                                        <th class="text-center">Resource Name</th>
                                        <th class="text-center" style="width: 120px;">View All</th>
                                        <th class="text-center" style="width: 120px;">View One</th>
                                        <th class="text-center" style="width: 120px;">Create</th>
                                        <th class="text-center" style="width: 120px;">Update</th>
                                        <th class="text-center" style="width: 120px;">Delete</th>
                                    </tr>
                                    @php
                                        $scope = $item['permission'] == 'access-cms' ? 'cms.' : 'mobile.';
                                    @endphp
                                    @foreach ($item['children'] as $child)
                                        @if ($this->canManageNode($child))
                                            <tr>
                                                @php
                                                    $group = $this->getPermissionGroupName($child['permission'], 'raw');
                                                    $viewAny = $scope .$group .'.viewAny';
                                                    $view = $scope .$group .'.view';
                                                    $create = $scope .$group .'.create';
                                                    $update = $scope .$group .'.update';
                                                    $delete = $scope .$group .'.delete';
                                                @endphp
                                                <td>{{ $this->getPermissionGroupName($child['permission']) }}</td>
                                                <td style="width: 120px;">
                                                    @if (in_array($viewAny, $this->allPermissions))
                                                        <label class="checkbox checkbox-outline checkbox-primary" style="width: 20px; overflow: hidden; margin: 0 auto;">
                                                            <input wire:model="permissions" type="checkbox" name="cms-{{ $group }}-viewAny" value="{{ $viewAny }}"><span></span>
                                                        </label>
                                                    @endif
                                                </td>
                                                <td style="width: 120px;">
                                                    @if (in_array($view, $this->allPermissions))
                                                        <label class="checkbox checkbox-outline checkbox-primary" style="width: 20px; overflow: hidden; margin: 0 auto;">
                                                            <input wire:model="permissions" type="checkbox" name="cms-{{ $group }}-view" value="{{ $view }}"><span></span>
                                                        </label>
                                                    @endif
                                                </td>
                                                <td style="width: 120px;">
                                                    @if (in_array($create, $this->allPermissions))
                                                        <label class="checkbox checkbox-outline checkbox-primary" style="width: 20px; overflow: hidden; margin: 0 auto;">
                                                            <input wire:model="permissions" type="checkbox" name="cms-{{ $group }}-create" value="{{ $create }}"><span></span>
                                                        </label>
                                                    @endif
                                                </td>
                                                <td style="width: 120px;">
                                                    @if (in_array($update, $this->allPermissions))
                                                        <label class="checkbox checkbox-outline checkbox-primary" style="width: 20px; overflow: hidden; margin: 0 auto;">
                                                            <input wire:model="permissions" type="checkbox" name="cms-{{ $group }}-update" value="{{ $update }}"><span></span>
                                                        </label>
                                                    @endif
                                                </td>
                                                <td style="width: 120px;">
                                                    @if (in_array($delete, $this->allPermissions))
                                                        <label class="checkbox checkbox-outline checkbox-primary" style="width: 20px; overflow: hidden; margin: 0 auto;">
                                                            <input wire:model="permissions" type="checkbox" name="cms-{{ $group }}-delete" value="{{ $delete }}"><span></span>
                                                        </label>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </table>
                            @endif
                        @endforeach

                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-primary">Update Role</button>
                            <button wire:click="backToIndex()" type="button" class="btn btn-light-primary ml-2">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
